from pkgutil import extend_path

__path__ = extend_path(__path__, __name__)

"""
copyright https://github.com/malfaux/snakecheese/tree/master/gadjolib
"""
