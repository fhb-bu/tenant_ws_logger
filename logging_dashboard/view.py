from django.shortcuts import  render_to_response
from django.contrib.auth import logout
from django.contrib.auth.decorators import user_passes_test
from tenant_ws.decorators import usingWebsockets

def getRole(request):
    if request.user.is_superuser:
        role = "Superuser"
    elif request.user.is_staff:
        role = "Staff"
    else:
        role = "User"
    return role

@usingWebsockets
@user_passes_test(lambda u: u.is_superuser)
def DashboardView(request):
    context=({'title' : 'Dashboard',
              'username' : request.user.username,
                       'role' : getRole(request)})
    sitename = "tenants/dashboard/dashboard.html"
    response = render_to_response(sitename,context)
    return response


def LogView(request):
    context=({'title' : 'User'})
    sitename = "user.html"
    response = render_to_response(sitename,context)
    return response

def PlaceholderView(request):
    sitename = "tenants/utility/placeholder.html"
    context=({'title' : 'Placeholder',
              'username' : request.user.username,
                'role' : getRole(request)})
    response = render_to_response(sitename,context)
    return response

def LogoutView(request):
    sitename = "tenants/utility/logout.html"
    context = ({})
    response = render_to_response(sitename,context)
    logout(request)
    return response

def IndexView(request):
    sitename = "tenants/base/index.html"
    context=({'title' : 'Index'})
    response = render_to_response(sitename,context)
    return response