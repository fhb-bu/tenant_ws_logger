import logging
from tenant_schemas.utils import get_public_schema_name
from django.conf import settings
from gadjo.requestprovider.signals import get_request
class WebsocketLoggingHandler(logging.Handler):
    def emit(self, record):
        message = {
            "loglevel":record.levelname,
            "date": record.created,
            "message": "{}".format(self.format(record))
        }
        request = get_request()
        if not request:
            return False
        from logging_dashboard import models

        if request.user.username == "":
            request.user.username = settings.ANONYMER_USERNAME
        if not request.tenant.schema_name == get_public_schema_name():
            models.LogMessages.addLog(request.user.username,message)
            message.update({
                "tenant_id": request.tenant.schema_name,
                "user": request.user.username,
            })
            from tenant_ws.wsSettings import WS_PARSER
            WS_PARSER.get("logger").sendMessage("add_log",
                                                message,
                                                "log",
                                                request.tenant.schema_name,
                                                request.user.username)


