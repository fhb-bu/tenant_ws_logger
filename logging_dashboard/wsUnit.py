from tenant_ws.wsSkeleton import wsSkeleton, wsGroups
from logging_dashboard.models import WebsocketDBHandler, UserInfo, Loggeduser, LogMessages
from channels import Group
import logging

logger = logging.getLogger('django.request')


class wsLogger(wsSkeleton):
    def __init__(self):
        wsSkeleton.__init__(self)
        self.path = "logger"
        self.shouldOverwriting = False
        self.incoming = {
            "loggingstatus":{
                "user":str,
                "state":int
            }
        }
        self.outgoing = {
            "add_log":{
                "tenant_id":str,
                "user":str,
                "loglevel":str,
                "date":float,
                "message":str,
            },
            "update_userlist":{
                "username":str,
                "onlinestate":bool,
                "state":int,
            },
            "log_messages":{
              "logmessages":list,
            },
            "userlist":{
                "userlist":list,
            },
        }
        self.groups = {
            "status" : wsGroups(False,True),
            "command" : wsGroups(True,True),
            "log" : wsGroups(False,False)
        }

    def onConnect(self,reply_channel,info_channel):
        self.sendMessage(
            "userlist",
            WebsocketDBHandler.generateUserList(info_channel.get("user")),
            "status",
            info_channel.get("tenant"),
            info_channel.get("user")
        )

    def handleMessage(self,cmd,message,reply_channel,info_channel):
        tenant = info_channel.get("tenant")
        user = info_channel.get("user")

        if cmd == "loggingstatus":
            if message.get("state"):
                Loggeduser.addUser(
                    user,
                    message.get("user"))
                Group(self.generateGroupName("log",tenant,message.get("user"))).add(reply_channel)
                self.sendMessage("log_messages",
                                 LogMessages.getLogs(message.get("user")),
                                 "log",
                                 tenant,
                                 message.get("user"))
            elif not message.get("state"):
                Loggeduser.delUser(
                               user,
                               message.get("user"))
                Group(self.generateGroupName("log",tenant,message.get("user"))).discard(reply_channel)
            self.sendMessage(
                "update_userlist",
                WebsocketDBHandler.updateUserlist(
                    message.get("user"),
                    UserInfo.getOnlineState(message.get("user")),
                    message.get("state")
                ),
                "command",
                tenant,
                user)

    #Signals
    def setOnlineState(self,sender, user, request, **kwargs):
        if not request.tenant.schema_name == 'public':
            onlineState = UserInfo.setOnlineState(username=user.username)
            self.sendMessage("update_userlist",
                             WebsocketDBHandler.updateUserlist(
                                    user.username,
                                    onlineState,
                                    -1
                             ),
                            "status",
                            request.tenant.schema_name)
            if onlineState:
                logger.info("Login")
            else:
                logger.info("Logout")
