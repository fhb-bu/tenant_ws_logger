# Websocket Loggingsystem

Websocket Loggingsystem is an Extention for the Framework Django to us the intern Loggingsystem from Django with Realtime Websockets. 

## Features
  - Full Integration of an expandable Framework for Websockets based
    on Django-Channels which is seperatet in an other Project
  - Full Support for Tenants with own Admininterface
  - Interactiv Dashboard written in HTML5 and some Javascript to
    Display all Logging Events up-to-date
  - Automated Setup for Tenants and Defaultuser, you also can do this 
    manually, but its harder and some migrations tasks can fail

## Requirements
  - A [redis server](https://redis.io) 

## Installation
First of all you must download the Sourcecode with git

```sh
git clone git@bitbucket.org:fhb-bu/tenant_ws_logger.git
```

For Installing necessary Python Pakages us the requirements.txt

```sh
pip install -r requirements.txt
```

its recommendable to setup a vitual environment to seperate this project 
from the normal python installation, but it is not necessary 

```sh
virtualenv -p python3 envname
cd envname/bin
source activate
```

now you have to modified the settings.py to fit with your Database and required Usersettings. Of cource you must use and setup an PostgreSQL Database
which is not part of the dokumentation

To setup the Database Entrys and recommend Users- and Tenantssettings you need to use the manage.py 

```sh
python manage.py setup
```

After the setup you also can use the manage.py to start the server in development mode 

```sh
python manage.py runserver
```

If the server running, you can open your Browser and login to the admininterface using the following URL. The default user is root and the password is 1234. Its highly recommened that you change the password 

```sh
127.0.0.1:8000/login
```

At this interface you can manage easyly the tenants. In an Development Environment you need to route your tenants to localhost in the file **/ect/hosts** like this:

```sh
127.0.0.1   tenant1.yourproject.com
```

The interface automatically creates an root and an anonymous user for the tenants. The anonymous user is only for intern reasons and should not use as real user. With the root user you can login in the normal Djangoadmin interface for each tenant like this and add some users.

```sh
tenant1.yourproject.com/admin
```

Now you can go to 

```sh
tenant1.yourproject.com
```

and navigate to logger for the Websocket-Logger-Dashboard

```sh
tenant1.yourproject.com/logger
```