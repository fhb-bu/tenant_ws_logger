class DragAndDrop{
        constructor(){
    }
    static allowDrop(ev) {
        ev.preventDefault();
    }
    static drag(ev) {
        ev.dataTransfer.setData("text", ev.target.id);
        ev.dataTransfer.setData("parent",ev.target.parentNode.id);
    }
    static drop(ev) {
        ev.preventDefault();
        var parent = document.getElementById(ev.target.id);
        var old = document.getElementById(DragAndDrop.getChild(parent));
        var share = document.getElementById(ev.dataTransfer.getData("text"));
        if (old !== null){
        document.getElementById(ev.dataTransfer.getData("parent")).appendChild(old);
        }
        ev.target.appendChild(share);
    }
    static getChild(parent){
        var children = parent.childNodes;
        for(let i = 0; i< children.length;i++){
            if(children[i].id !== undefined){
                return children[i].id}
        }
    }
}