wsHandler = new WsBase("logger");

class handleWS{
    constructor() {}
    static handleMessage(msg){
        if (msg.cmd == 'add_log'){
            Controller.addLogMessage(msg.user,msg.loglevel,msg.date, msg.message)
        };
        if (msg.cmd == 'update_userlist'){
            Controller.updateUserOverview(msg.username,msg.onlinestate,msg.state);
        };
        if (msg.cmd == 'log_messages'){
            Controller.addMessages(msg.logmessages);
        };
        if (msg.cmd == 'userlist'){
            Controller.generateUserOverview(msg.userlist);
        };
    };
    static send_loggingstatus(user, state) {
        if (state) {
            Controller.addWidget(user, "LogWidget");
            log_manager.addUser(user);
        }
        else {
            Controller.deleteWidget(user);
                log_manager.delUser(user);
        }
        if (wsHandler.sock) {
            wsHandler.sock.send(JSON.stringify({cmd: 'loggingstatus', user: user, state: state}));
        }
        else {
            console.log("cannot send logging status over Websocket")
            }
        }
}