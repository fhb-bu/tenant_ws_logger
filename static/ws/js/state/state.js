wsHandler = new WsBase("state");

class handleWS{
    constructor() {}
    static handleMessage(msg){
        if (msg.cmd === 'state'){
            Controller.handleSystemData(msg);
        }
        if (msg.cmd === 'temperature'){
            Controller.handleSensorData(msg);
        }
        if (msg.cmd === 'processes'){
            Controller.handleProcesslistData(msg);
        }
    };
    static send_start(task) {
        if (wsHandler.sock) {
            wsHandler.sock.send(JSON.stringify({cmd: 'start', task: task}));
        }
        else {
            console.log("cannot send Task over Websocket")
            }
        }
    static send_stop(task) {
        if (wsHandler.sock) {
            wsHandler.sock.send(JSON.stringify({cmd: 'stop', task: task}));
        }
        else {
            console.log("cannot send Task over Websocket")
            }
        }
}