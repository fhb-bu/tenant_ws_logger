class WsBase{
    constructor(path) {
        this.sock = undefined;
        this.uri = undefined;
        if (window.location.protocol === "file:") {
            this.uri = "ws://127.0.0.1:8000/" + path;
        }
        else {
            this.uri = "ws://" + window.location.hostname + ":8000/" + path;
        }
        if ("WebSocket" in window) {
            this.sock = new WebSocket(this.uri);
        }
        else if ("MozWebSocket" in window) {
            this.sock = new MozWebSocket(this.uri);
        }
        else {
            console.log("Browser does not support WebSocket!");
        }
        if (this.sock) {
            this.sock.onopen = function () {
                console.log("Connected to " + this.uri);
            };
            this.sock.onclose = function (e) {
                console.log("Connection closed (reason = '" + e.reason + "')");
                this.sock = null;
            };
            this.sock.onmessage = function (e) {
                var msg = JSON.parse(e.data);
                console.log('WSMessages: ' + JSON.stringify(msg));
                handleWS.handleMessage(msg);
            }
        }
    }
}