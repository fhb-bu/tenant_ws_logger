from django_celery_beat.models import IntervalSchedule, PeriodicTask
import json
from channels import Group
from django.conf import settings

period = {
    "days":IntervalSchedule.DAYS,
    "hours":IntervalSchedule.HOURS,
    "minute":IntervalSchedule.MINUTES,
    "seconds":IntervalSchedule.SECONDS,
    "microsecounds":IntervalSchedule.MICROSECONDS
}


"""
interval = {
    "rate": Integer,
    "period": String from Dict period
}
task = String
ws_options = {
    "cmd" : String,
    "tenant" : String,
    "user" : String,
    "path" : String,
    "type" : String (
                    Shared
                    Tenant
                    User)
}

"""

class TaskManager:
    @classmethod
    def extractGroupname(cls,task):
        task = task.split(".")
        return task[len(task)-1]

    @classmethod
    def generateGroupName(cls, task,type, tenant=None, username=None):
        groupname = TaskManager.extractGroupname(task)
        if  not type =="shared":
            groupname += settings.SEPERATOR + tenant
        if type =="user":
            groupname += settings.SEPERATOR + username
        return groupname

    @classmethod
    def addTask(cls, reply_channel, task, ws_options, params):
        alias = settings.TASKALIAS.get(task)
        taskDBObject = None
        groupname = TaskManager.generateGroupName(
            alias.get("task"),
            alias.get("type"),
            ws_options.get("tenant"),
            ws_options.get("user"),
        )
        try:
            taskDBObject = PeriodicTask.objects.get(name=groupname)
        except:
            schedule, created = IntervalSchedule.objects.get_or_create(
                every=alias.get("rate"),
                period=period.get(alias.get("period")),
            )
            PeriodicTask.objects.create(
                interval=schedule,
                name=groupname,
                task=alias.get("task"),
                kwargs=json.dumps({
                    "tenant": ws_options.get("tenant"),
                    "user": ws_options.get("user"),
                    "path": ws_options.get("path"),
                    "cmd": alias.get("cmd"),
                    "group": groupname,
                    "params": params
                }),
            )
        if alias.get("type") != "user":
            args = json.loads(taskDBObject.args)
            if not ws_options.get("user") in taskDBObject.args:
                args.append(ws_options.get("user"))
                taskDBObject.args = json.dumps(args)
                taskDBObject.save()

        Group(groupname).add(reply_channel)
    @classmethod
    def delTask(cls,reply_channel, task, ws_options):
        alias = settings.TASKALIAS.get(task)
        groupname = TaskManager.generateGroupName(
            alias.get("task"),
            alias.get("type"),
            ws_options.get("tenant"),
            ws_options.get("user"),
        )
        try:
            taskDBObject = PeriodicTask.objects.get(
                name=TaskManager.generateGroupName(
                    alias.get("task"),
                    alias.get("type"),
                    ws_options.get("tenant"),
                    ws_options.get("user")
                )
            )
            args = json.loads(taskDBObject.args)
            if ws_options.get("type") != "user":
                del args[ws_options.get("user")]
                taskDBObject.args = json.dumps(args)
                taskDBObject.save()
            if not (alias.get("type") != "user" and len(args) > 0):
                taskDBObject.delete()
        except:
            pass
        Group(groupname).discard(reply_channel)
