from channels.sessions import channel_session
from tenant_ws.wsSettings import WS_PARSER
from tenant_ws.wsUtils import WebsocketUtility
from channels.auth import channel_and_http_session_user_from_http
from tenant_ws.decorators import usingSessionData


@channel_and_http_session_user_from_http
@usingSessionData
def connect(message):
    accept = True
    if not "info" in message.channel_session:
        accept = False
    message.reply_channel.send({"accept": accept})
    WebsocketUtility.setConnection(
        message.channel_session['info'].get("tenant")
    )
    WS_PARSER.get(
        message.channel_session['info'].get("path")
    ).addGroups(
        message.reply_channel,
        message.channel_session['info']
    )
    WS_PARSER.get(
        message.channel_session['info'].get("path")
    ).onConnect(
        message.reply_channel,
        message.channel_session['info']
    )

@channel_session
def receive(message):
    WebsocketUtility.setConnection(message.channel_session['info'].get("tenant"))
    WS_PARSER.get(
        message.channel_session['info'].get("path")
    ).prepareMessage(
        message.content.get("text"),
        message.reply_channel,
        message.channel_session['info']
    )

@channel_session
def disconnect(message):
    if "info" in message.channel_session:
        WebsocketUtility.setConnection(message.channel_session['info'].get("tenant"))
        WS_PARSER.get(
            message.channel_session['info'].get("path")
        ).onClose(
            message.reply_channel,
            message.channel_session['info'].get("tenant")
            )
        WS_PARSER.get(
            message.channel_session['info'].get("path")
        ).delGroups(
            message.reply_channel,
            message.channel_session['info']
        )


