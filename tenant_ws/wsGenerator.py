from django.conf import settings
import os
from tenant_ws.wsSettings import WS_PARSER

JS_BASE = """
wsBase = new WsBase("$path");

class handleWS{
\tconstructor() {}
$in
$out
}
"""

HTML_HEADER = """
<title>{{ title }}</title>
{% load staticfiles %}
"""

class WsGenerator:
    def __init__(self):
        self.jsBaseFilePath = self.checkDir(os.path.join(settings.BASE_DIR,"static","ws","js"))
        self.templateDir = self.checkDir(os.path.join(settings.BASE_DIR,"templates","tenants"))
        self.generate()

    def generate(self):
        for path in WS_PARSER:
            if WS_PARSER[path].shouldOverwriting:
                self.generateJS(path)

    def generateJS(self,path):
        base = JS_BASE.replace("$path",path)
        base = base.replace("$in",self.generateJSIn(path))
        base = base.replace("$out", self.generateJSOut(path))
        file = open(os.path.join(self.jsBaseFilePath,path,path+".js"),"w")
        file.write(base)
        file.close()
        self.writeHeader(path)

    def generateJSIn(self,path):
        base = """\t\tif (msg.cmd == '$cmd'){};"""
        returnString = "\tstatic handleMessage(msg){\n"
        group = WS_PARSER.get(path).outgoing
        for entry in group:
            javaScript = base.replace("$cmd", entry)
            returnString += javaScript + "\n"
        returnString += "\t}"
        return returnString

    def generateJSOut(self,path):
        base = """\tstatic send_$cmd($header){sock.send(JSON.stringify({$body}))};"""
        returnString = ""
        group = WS_PARSER.get(path).incoming
        for entry in group:
            javaScript = base.replace("$cmd", entry)
            javaScript = javaScript.replace("$header", self.generateJSFunction(group[entry]))
            javaScript = javaScript.replace("$body", self.generateJSFunction(group[entry], True, entry))
            returnString += javaScript
        return returnString

    def generateJSFunction(self,payload,body=False,cmd=None):
        var = ""
        counter = 0
        if body:
            var+= "cmd: '" + cmd + "', "
        for line in payload:
            if not body:
                var += line
            elif body:
                var += line + ": "+line
            if counter < (len(payload)-1):
                var += ", "
            counter+=1
        return var

    def checkDir(self,dir):
        if not os.path.exists(dir):
            os.makedirs(dir)
        return dir

    def writeHeader(self,entry):
        data = HTML_HEADER
        template = """<script src="{% static "$path" %}"></script>\n"""
        data += template.replace("$path", os.path.join("ws","js", "ws_base.js"))
        data+= template.replace("$path",os.path.join(self.jsBaseFilePath,entry,entry+".js"))
        path = self.checkDir(os.path.join(self.templateDir,entry))
        file = open(os.path.join(path,"header.html"),"w")
        file.write(data)
        file.close()

