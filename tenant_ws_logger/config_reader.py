import json
import os
import sys

class ConfigReader:
    def __init__(self,path):
        i = 0
        for dir in os.listdir(path):
            new_path = os.path.join(path,dir,"config")
            if new_path.find(".") == -1 and os.path.exists(new_path) and os.path.isdir(new_path):
                self.path = new_path
                i += 1
        if i == 0:
            self.handleError("Config folder don't exist !")
        if i>1:
            self.handleError("Multible Config Folder!")
    def handleError(self,error):
        print(error)
        sys.exit(-1)
    def readConfig(self,filename):
        try:
            file = open(os.path.join(self.path,filename),"r")
            config = json.load(file)
            file.close()
        except:
            self.handleError("Error in Config {}".format(os.path.join(self.path,filename)))
        return config




