import os
from tenant_ws_logger.config_reader import ConfigReader
from collections import OrderedDict

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
configreader = ConfigReader(BASE_DIR)
DEBUG = True

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

BASE_URL = "trendy-sass.com"
ALLOWED_HOSTS = [
    "."+BASE_URL,
    "127.0.0.1"
]
DATABASES = configreader.readConfig("database.json")
SECRET_KEY = configreader.readConfig("secretkey.json").get("secretkey")


TIME_ZONE = 'Europe/Berlin'
LANGUAGE_CODE = 'de-de'

SITE_ID = 1

USE_I18N = True
USE_L10N = True
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = ''
# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = ''

STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static'),
]


# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)


DATABASE_ROUTERS = (
    'tenant_schemas.routers.TenantSyncRouter',
)

TEST_RUNNER = 'django.test.runner.DiscoverRunner'


MIDDLEWARE_CLASSES = (
    'tenant_schemas.middleware.TenantMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'logging_dashboard.middleware.AutoLogout',
    'gadjo.requestprovider.middleware.RequestProvider',
 )


ROOT_URLCONF = 'tenant_ws_logger.urls_tenants'
PUBLIC_SCHEMA_URLCONF = 'tenant_ws_logger.urls_public'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = ['django.template.loaders.filesystem.Loader',
 'django.template.loaders.app_directories.Loader']

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.contrib.messages.context_processors.messages'
            ],
        },
    },
]


SHARED_APPS = (
    'tenant_schemas',  # mandatory
    'tenant_ws_logger',
    'tenant_ws',        # For Generator
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'channels',  # Websocket
)

TENANT_APPS = (
    # The following Django contrib apps must be in TENANT_APPS
    'django.contrib.contenttypes',
    'django.contrib.auth',
    'django.contrib.admin',
    'logging_dashboard',
)

TENANT_MODEL = "tenant_ws_logger.Client"  # app.Model
DEFAULT_FILE_STORAGE = 'tenant_schemas.storage.TenantFileSystemStorage'





INSTALLED_APPS = INSTALLED_APPS = list(OrderedDict.fromkeys(SHARED_APPS + TENANT_APPS).keys())

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.

LOGGING_DIR_TENANT = os.path.join(os.path.abspath(os.path.dirname(__name__)), "tenants_log" + os.sep + "logging" + os.sep + '%s' + os.sep + 'Log.log')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'rest_log': {
            'level': 'DEBUG',
            'class': 'logging_dashboard.websocket_logger.WebsocketLoggingHandler',
            'formatter': 'verbose',
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['rest_log'],
            'level': 'DEBUG',
            'propagate': False,
        },
    }
}
# Websocket Settings


REDIS = {
    "host":"localhost",
    "port":"6379",
    "database":"0",
}

BROKER_URL = "redis://" + REDIS.get("host") + ":" + REDIS.get("port") + "/" + REDIS.get("database")

CHANNEL_LAYERS = {
   "default": {
       "BACKEND": "asgi_redis.RedisChannelLayer",
       "CONFIG": {
           "hosts":[BROKER_URL],
       },
       "ROUTING": "tenant_ws.routing.channel_routing",
   },
}


LOGIN_REDIRECT_URL = "/placeholder/"

# Auto logout Cookies
SESSION_EXPIRE_AT_BROWSER_CLOSE = True
SESSION_COOKIE_AGE = 15*60


# Auto logout Middleware
AUTO_LOGOUT_DELAY = 5

# Log App Settings

SEPERATOR = '_'

DEFAULT_SUPERUSER = "root"
DEFAULT_PASSWORD = "1234"
DEFAULT_PUBLIC_URL = "127.0.0.1"
ANONYMER_USERNAME = "Unknown"


accept_content = ['json']