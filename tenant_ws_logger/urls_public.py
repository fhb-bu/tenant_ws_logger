from django.conf.urls import url
from tenant_ws_logger.view import LoginView, AdminIndexView,IndexView
#from tenant_ws_logger.view import LogoutView


urlpatterns = [
    url(r'^$', IndexView),
    url(r'login/', LoginView,name="LoginView"),
    url(r'Index_Admin/', AdminIndexView,name="admin"),
#    url(r'logout/', LogoutView,name="logoutview"),
]

