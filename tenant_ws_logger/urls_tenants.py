from django.conf.urls import url , include
from logging_dashboard.view import DashboardView, PlaceholderView, LogoutView,IndexView
from django.contrib import admin
from django.contrib.auth.views import login


admin.autodiscover()



urlpatterns = [
    url(r'^$', IndexView),
    url(r'placeholder/', PlaceholderView,name="placeholder"),
    url(r'logger/', DashboardView,name="logview"),
    url(r'^admin/', include(admin.site.urls)),
    url(r'logout/', LogoutView,name="logoutview"),
    url(r'^accounts/login/$', login, {'template_name': 'tenants/utility/login.html'},name="login"),
]


